<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

    public function login($user, $pass)
    {
        $json = array(
            'msg' => '',
            'response' => false,
            'data' => ''
        );
        if ($this->input->get("user") != null && $this->input->get("pass") != null) {
            $user = $this->input->get("user");
            $pass = $this->input->get("pass");
        }

        $this->load->model('usuario');
        $data = $this->usuario->login($user, $pass);
        if ($data['usuario'] != null) {
            $_SESSION['login'] = $data['usuario']->user;
            $json['data'] = $data;
            $json['response'] = true;
        } else {
            $json['msg'] = "Acceso denegado";
        }
        echo json_encode($json);
        return;
    }

    public function logout()
    {
        $json = array(
            'msg' => '',
            'response' => false,
            'data' => ''
        );
        $_SESSION['login'] = null;
        if(!$_SESSION['login']){
            $json['response'] = true;
            $json['msg'] = "Logout sobre API1.0 realizado";
        }
        else{
            $json['msg'] = "¡Lo siento, hay un problema!";
        }
        echo json_encode($json);
        return;
    }
}
