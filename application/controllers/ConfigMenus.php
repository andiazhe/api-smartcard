<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ConfigMenus extends CI_Controller
{
    
    public function get_navbars()
    {
        $json = array(
            'msg' => '',
            'response' => false,
            'data' => ''
        );
        $ciudad_id = 1;
        if ($this->input->get("ciudad_id") != null) {
            $ciudad_id = $this->input->get("ciudad_id");
        }

        $this->load->model('rolenavbar');
        $role_navbars = $this->rolenavbar->get_navbars_by_role(1);
        $ids = array();
        if ($role_navbars['role_navbars'] != null) {
            foreach($role_navbars['role_navbars'] as $role_navbar){
                $ids[$role_navbar->navbar_id] = $role_navbar->navbar_id;
            }
        }
        else{
            $json['msg'] = "No hay navbars para ese rol de usuario";
            echo json_encode($json);
            return;
        }

        $this->load->model('navbar');
        
        $data = $this->navbar->get_navbar_id($ids);
        if ($data['navbars'] != null) {
            $json['data'] = $data;
            $json['response'] = true;
        } else {
            $json['msg'] = "Navbars no disponibles para el usuario";
        }
        echo json_encode($json);
        return;
    }

    public function get_menus_items(){
        $json = array(
            'msg' => '',
            'response' => false,
            'data' => ''
        );
        $ciudad_id = 1;
        if ($this->input->get("ciudad_id") != null) {
            $ciudad_id = $this->input->get("ciudad_id");
        }
        $this->load->model('menuconfig');
        $data = $this->menuconfig->get_navbars_by_role_navbar(1, 1);
        if ($data['menus_config'] != null) {
            $json['data'] = $data;
            $json['response'] = true;
        } else {
            $json['msg'] = "Menus no disponibles para el usuario";
        }
        echo json_encode($json);
        return;
    }
}
