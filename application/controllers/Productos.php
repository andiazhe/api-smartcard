<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {

	public function get_productos()
	{
        $json = array(
            'msg' => '',
            'response' => false,
            'data' => ''
        );
        $ciudad_id = 1;
        if ( $this->input->get("ciudad_id")!=null ) {
            $ciudad_id = $this->input->get("ciudad_id");
        }
        
        $this->load->model('producto');
        $data = $this->producto->getAllProductos();
        if($data['productos'] != null){
            $json['data'] = $data;
            $json['response'] = true;
        }
        else {
            $json['msg'] = "No hay productos";
        }
        echo json_encode($json);
        return;
	}
}
