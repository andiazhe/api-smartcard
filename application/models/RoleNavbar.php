<?php

class RoleNavbar extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function get_navbars_by_role($role){
        $this->db->where(
            array(
                'rol_id' => $role
            )
        );
        $role_navbars = null;
        $query = $this->db->get('config_role_navbars');
        $role_navbars['role_navbars'] = $query->result();
        return $role_navbars;
    }
}