<?php

class MenuConfig extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function get_navbars_by_role_navbar($role, $navbar){
        $this->db->where(
            array(
                'role_id' => $role
            )
        );
        $query = $this->db->get('config_menu');
        $menus_config['menus_config'] = $query->result();
        return $menus_config;
    }
}