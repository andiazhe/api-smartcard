<?php

class Producto extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function getAllProductos(){
        $productos = null;
        $query = $this->db->get('productos');
        $productos['productos'] = $query->result();
        return $productos;
    }
}