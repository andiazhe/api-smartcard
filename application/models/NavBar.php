<?php

class Navbar extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function get_navbar_id($arrayIds){
        $query = "SELECT * FROM `config_navbar` WHERE id IN (".implode(",", $arrayIds).")";
        $query = $this->db->query($query);
        $navbars['navbars'] = $query->result();
        return $navbars;
    }
}